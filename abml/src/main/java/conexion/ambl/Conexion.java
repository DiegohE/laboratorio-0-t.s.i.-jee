package conexion.ambl;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;

//Pool de conexiones
public class Conexion {
	private static BasicDataSource dataSource=null;
	
	private static DataSource getDataSource() {
		if(dataSource==null) {
			dataSource = new BasicDataSource();
			//Propiedades
			dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
			dataSource.setUsername("javaee");
			dataSource.setPassword("12345678");
			dataSource.setUrl("jdbc:mysql://localhost:3306/abml");
			//Otras propiedades básicas
			dataSource.setInitialSize(20);//Con cuantas conexiones inicia el pool.
			dataSource.setMaxIdle(15);//Cuantas conexiones pueden estar activas.
			dataSource.setMaxTotal(20);//Total de conexiones que pueden haber.
			dataSource.setMaxWaitMillis(5000);//Tiempo en milisegundos que debe esperar hasta definir que una conexión está inactiva y cerrarla.
		}
		return dataSource;
	}
	
	public static Connection getConnection() throws SQLException {
		return getDataSource().getConnection();
	}
}
