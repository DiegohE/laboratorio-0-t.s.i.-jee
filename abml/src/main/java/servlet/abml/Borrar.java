package servlet.abml;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ambl.PersonalDAO;

/**
 * Servlet implementation class Borrar
 */
@WebServlet(description = "Se encarga de todo lo relacionado con el borrado de usuarios registrados.", urlPatterns = { "/Borrar" })
public class Borrar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Borrar() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PersonalDAO personalDAO=new PersonalDAO();
		String correo = request.getParameter("email");
		
		try {
			if(personalDAO.obtenerPersonal(correo)!=null) {
				personalDAO.borrarPersonal(correo);
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("/borradoOK.html");
				requestDispatcher.forward(request, response);	
			} else {
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("/borradoEXIST.html");
				requestDispatcher.forward(request, response);		
			}
		} catch (SQLException e) {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/borradoERR.html");
			requestDispatcher.forward(request, response);
			e.printStackTrace();
		}
	}

}
