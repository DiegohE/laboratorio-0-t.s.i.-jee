package servlet.abml;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ambl.PersonalDAO;

/**
 * Servlet implementation class Abml
 */
@WebServlet("/Abml")
public class Abml extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Abml() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter pw;
		response.setContentType("text/html");
		pw = response.getWriter();

		String nombre = request.getParameter("first-name");
		String apellido = request.getParameter("last-name");
		String correo = request.getParameter("email");
		String numero = request.getParameter("phone");
		PersonalDAO personalDAO = new PersonalDAO();
		try {
			boolean agregoPersonal = personalDAO.AgregarPersonal(nombre, apellido, correo, numero);
			if(agregoPersonal == true) {
				response.setStatus(response.SC_MOVED_TEMPORARILY);
				response.setHeader("Location", "invalido.html");
			} else {
				response.setStatus(response.SC_MOVED_TEMPORARILY);
				response.setHeader("Location", "validoTick.html");
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
