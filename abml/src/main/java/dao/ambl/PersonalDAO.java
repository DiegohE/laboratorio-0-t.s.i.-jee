package dao.ambl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conexion.ambl.Conexion;
import model.abml.Personal;

//Acceso a base de datos
public class PersonalDAO {
	private Connection connection;
	private PreparedStatement statement;
	private boolean estadoOperacion;
	//Obtener Conexion pool
	private Connection obtenerConexion() throws SQLException {
		return Conexion.getConnection();
	}
	public List<Personal> listaPersonal() throws SQLException {
		ResultSet resultSet=null;
		String sql=null;
		List<Personal> listaPersonal=new ArrayList<>();
		estadoOperacion=false;
		connection=obtenerConexion();
		
		try {
			sql="SELECT * FROM personal";
			statement=connection.prepareStatement(sql);
			resultSet=statement.executeQuery(sql);
			while(resultSet.next()) {
				Personal p=new Personal();
				p.setNombre(resultSet.getString("nombre"));
				p.setApellido(resultSet.getString("apellido"));
				p.setCorreo(resultSet.getString("correo"));
				p.setTelefono(resultSet.getString("telefono"));
				listaPersonal.add(p);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listaPersonal;
	} 
	public boolean AgregarPersonal(String nombre, String apellido, String correo, String telefono) throws SQLException {
		boolean personalExiste = false;
		connection=obtenerConexion();
		
		try {

			PreparedStatement ps = connection.prepareStatement("SELECT correo from personal where correo = '" + correo + "'");
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				// Usuario Existe
				personalExiste = true;
			} else {
				// Usuario no existe
				personalExiste = false;
				String query = "Insert into personal(nombre,apellido, correo, telefono) values (?,?,?,?);";
				PreparedStatement pstmt = connection.prepareStatement(query);
				pstmt.setString(1, nombre);
				pstmt.setString(2, apellido);
				pstmt.setString(3, correo);
				pstmt.setString(4, telefono);
				pstmt.executeUpdate();

			}


		} catch (Exception e) {
			e.printStackTrace();
		}
		return personalExiste;
	}
	//Borrar un registro mediante su correo.
	public boolean borrarPersonal(String correo) throws SQLException {
		String sql=null;
		estadoOperacion=false;
		connection=obtenerConexion();
		try {
			connection.setAutoCommit(false);//indica inicio de transaccion.
			sql="DELETE FROM personal WHERE correo=?";
			statement=connection.prepareStatement(sql);
			statement.setString(1, correo);
			//En caso de ejecutarse correctamente, se retorna valor mayor que 0
			//almacenándose true en la variable.
			//En caso contrario, se mantiene el false con el que fue inicializada
			//dicha variable.
			estadoOperacion=statement.executeUpdate()>0;
			connection.commit();
			statement.close();
			connection.close();//Se devuelve la conexión al pool de conexiones.
		} catch (SQLException e) {
			connection.rollback();//En caso de error, se devuelve al estado anterior.
			e.printStackTrace();
		}
		return estadoOperacion;
	}
	//Obtener un registro mediante su correo o null en caso de que no exista.
	public Personal obtenerPersonal(String correo) throws SQLException {
		ResultSet resultSet = null;
		Personal personal=null;
		String sql = null;
		estadoOperacion=false;
		connection = obtenerConexion();
		try {
			sql = "SELECT * FROM personal WHERE correo=?";
			statement=connection.prepareStatement(sql);
			statement.setString(1, correo);
			resultSet=statement.executeQuery();
			//Si la sentencia sql no devolvió nada entonces
			//la variable personal se mantiene en null.
			if (resultSet.next()) {
				personal=new Personal();
				personal.setNombre(resultSet.getString("nombre"));
				personal.setApellido(resultSet.getString("apellido"));
				personal.setCorreo(resultSet.getString("correo"));
				personal.setTelefono(resultSet.getString("telefono"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return personal;
	}
}
